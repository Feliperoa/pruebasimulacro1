package navdrawer.test.com.navigationdrawertest.utils.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import navdrawer.test.com.navigationdrawertest.R;
import navdrawer.test.com.navigationdrawertest.model.Receta;

/**
 * Created by Felipe on 14/07/2017.
 */

public class RecetaAdapter extends RecyclerView.Adapter<RecetaAdapter.RecetaaViewHolder> {
    List<Receta> list;
    Context context;

    public interface OnItemClick {
        void OnClick(Receta receta, int position);
    }

    OnItemClick listener = null;

    public RecetaAdapter(List<Receta> list, Context context, OnItemClick listener) {
        this.list = list;
        this.context = context;
        this.listener = listener;
    }

    public static class RecetaaViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView name;
        View vi;
        public RecetaaViewHolder(View v) {
            super(v);
            vi = v;
            img = (ImageView) v.findViewById(R.id.item_photo);
            name = (TextView) v.findViewById(R.id.item_name);
        }

        public void bind(final Receta receta, final int positio, final OnItemClick onItemClick) {
            name.setText(receta.getName());
            if (receta.getImagen() != null) {
                if (receta.getImagen() != "") {
                    try {
                        Log.e("LOGG IMG", "imagen: " + receta.getImagen());
                        InputStream stream = vi.getContext().getAssets().open(receta.getImagen());
                        Drawable d = Drawable.createFromStream(stream, null);
                        if (d !=null) {
                            img.setImageDrawable(d);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.OnClick(receta, positio);
                }
            });

        }
    }
    @Override
    public RecetaaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_receta, parent, false);
        RecetaaViewHolder recetaaViewHolder = new RecetaaViewHolder(view);

        return recetaaViewHolder;
    }

    @Override
    public void onBindViewHolder(RecetaaViewHolder holder, int position) {
        holder.bind(list.get(position), position, listener);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
