package navdrawer.test.com.navigationdrawertest.controller;

import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import navdrawer.test.com.navigationdrawertest.fragments.NavigationDrawerFragment;
import navdrawer.test.com.navigationdrawertest.R;
import navdrawer.test.com.navigationdrawertest.model.Receta;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {
    public static String category;
    List<String> files;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Recetas");
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.main_layout));

        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_layout);

        files = new ArrayList<>();
        for (int e=1; e<=67;e++) {
            files.add("t"+e+".txt");
        }

        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();*/

       /* NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);*/
        if (files.size() > 0) {
            for (int o =0;o<files.size();o++) {
                readFiles(files.get(o));
            }
        }
    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
*/
    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/
    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments

    }
    public void readFiles(String file) {
        List<Receta> exist = Receta.find(Receta.class, "file = ?", new String[]{file});
        if (exist.size() == 0) {
            List<String> ingredientes = new ArrayList<>();
            List<String> lineas = new ArrayList<>();
            Log.e("LOGG", "Entra aqui");
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(getAssets().open(file)));
                String mLine;
                while ((mLine = reader.readLine()) != null) {
                    lineas.add(mLine);
                }
                Log.e("LOGG", lineas.toString());
                if (lineas.size() > 0) {
                    int type = 0;
                    int pos_ingre = 0;
                    int prepa = 0;
                    int nombre = 0;
                    int tiempo = 0;
                    int origen = 0;
                    int imagen = 0;
                    int categoria = 0;
                    Receta receta = new Receta();
                    receta.setFile(file);
                    for (int i = 0; i < lineas.size(); i++) {
                        String line = lineas.get(i);
                        switch (line) {
                            case "Nombre de la receta:":
                                //Log.e("LOGG", "Si entra al nombre");
                                receta.setName(lineas.get(1));
                                nombre = i;
                                break;
                            case "Lista de Ingredientes:":
                                //Log.e("LOGG", "Si entra a ingredientes : "+i);
                                pos_ingre = i;
                                break;
                            case "PREPARACIÓN:":
                                //Log.e("LOGG", "Si entra a preparación : "+i);
                                prepa = i;
                                break;
                            case "TIEMPO DE PREPARACIÓN:":
                                // Log.e("LOGG", "Si entra a tiempo preparación : "+i);
                                tiempo = i;
                                break;
                            case "ORIGEN:":
                                // Log.e("LOGG", "Si entra a origen : "+i);
                                origen = i;
                                break;
                            case "IMAGEN:":
                                //  Log.e("LOGG", "Si entra a imagen : "+i);
                                imagen = i;
                                break;
                            case "CATEGORÍA:":
                                // Log.e("LOGG", "Si entra a categoria : "+i);
                                categoria = i;
                                break;
                        }
                    }
                    String inged = "";
                    if (pos_ingre > 0 && prepa > 0) {
                        Log.e("Logg", "entra al if");
                        for (int i = pos_ingre + 1; i < prepa; i++) {
                            String ingrediente = lineas.get(i);
                            inged = inged.concat("\n" + ingrediente);
                            //ingredientes.add(ingrediente);
                        }
                        receta.setIngredientes(inged);
                        if (ingredientes.size() > 0) {
                           // receta.setIngredientes(ingredientes);
                        }
                        Log.e("LOGG", "lista de ingredientes:" + ingredientes.toString());

                    }
                    if (prepa > 0 && tiempo > 0) {
                        String prepaa = "";
                        Log.e("Logg", "entra al if prepa");
                        for (int i = prepa + 1; i < tiempo; i++) {
                            prepaa = prepaa.concat("\n" + lineas.get(i));
                        }
                        receta.setDescripcion(prepaa);
                        Log.e("LOGG", "lista de prepa:" + prepaa);
                    }
                    if (tiempo > 0) {

                        String tie = lineas.get(tiempo + 1);
                        Log.e("LOGG", "el tiempo: " + tie);
                        receta.setTiempo(tie);
                    }
                    if (origen > 0) {
                        String or = lineas.get(origen + 1);
                        Log.e("LOGG", "el origen: " + or);
                        receta.setOrigen(or);
                    }
                    if (imagen > 0) {
                        String or = lineas.get(imagen + 1);
                        Log.e("LOGG", "la iamgen: " + or);
                        receta.setImagen(or);
                    }
                    if (categoria > 0) {
                        String or = lineas.get(categoria + 1);
                        Log.e("LOGG", "el catgoria: " + or);
                        receta.setCategoria(or);
                    }
                    receta.save();

                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        } else {
            Log.e("LOGG", "ya esta guardada la receta");
        }
    }



}
