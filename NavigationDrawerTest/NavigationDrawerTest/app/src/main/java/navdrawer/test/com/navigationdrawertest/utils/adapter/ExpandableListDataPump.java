package navdrawer.test.com.navigationdrawertest.utils.adapter;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ExpandableListDataPump {
    public static HashMap<String, List<String>> getData() {
        LinkedHashMap<String, List<String>> expandableListDetail = new LinkedHashMap<String, List<String>>();

        List<String> menu3 = new ArrayList<String>();
        List<String> menu2 = new ArrayList<String>();
        List<String> menu4 = new ArrayList<String>();
        List<String> menu5 = new ArrayList<String>();
        List<String> menu6 = new ArrayList<String>();
        List<String> menu7 = new ArrayList<String>();

        List<String> list1 = new ArrayList<String>();
        list1.add("Almuerzo");
        list1.add("Desayuno");
        list1.add("Cena");


        List<String> list2 = new ArrayList<String>();
        list2.add("Arroces");
        list2.add("Patas");
        List<String> list3 = new ArrayList<String>();
        list3.add("CarnesRojas");
        list3.add("Aves");
        list3.add("Pescados");
        List<String> list4 = new ArrayList<String>();
        list4.add("Cumpleaños");
        list4.add("Fiestas infantiles");
        list4.add("Ensaladas");
        list4.add("Parrilladas");
        list4.add("Postres");
        list4.add("Queso");
        list4.add("Semana Santa");
        List<String> list5 = new ArrayList<String>();
        list5.add("15 minutos");
        list5.add("25 minutos");



        expandableListDetail.put("Inicio", menu2);
        expandableListDetail.put("Comidas diarias", list1);
        expandableListDetail.put("Granos", list2);
        expandableListDetail.put("Carnes", list3);
        expandableListDetail.put("Especiales", list4);
        expandableListDetail.put("Recetas rápidas", list5);
        expandableListDetail.put("Búsqueda por Ingredientedas", menu6);
        expandableListDetail.put("Acercade",menu7);


        return expandableListDetail;
    }
}
