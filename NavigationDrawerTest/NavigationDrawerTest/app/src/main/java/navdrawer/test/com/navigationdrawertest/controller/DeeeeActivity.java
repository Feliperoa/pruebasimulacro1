package navdrawer.test.com.navigationdrawertest.controller;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

import navdrawer.test.com.navigationdrawertest.R;
import navdrawer.test.com.navigationdrawertest.model.Receta;

public class DeeeeActivity extends AppCompatActivity {
Receta receta;
    CollapsingToolbarLayout collapsingToolbarLayout;
    TextView origen,ingredientes,descripcion,tiempo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deeee);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
              /*  Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });
        origen = (TextView) findViewById(R.id.txtorigen);
        ingredientes = (TextView) findViewById(R.id.txtingredientes);
        descripcion = (TextView) findViewById(R.id.txtdescripcion);
        tiempo = (TextView) findViewById(R.id.txttiempoestiamdo);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            receta = Receta.findById(Receta.class,Long.parseLong(b.getString("id")));
            collapsingToolbarLayout.setTitle(receta.getName());
            origen.setText(receta.getOrigen());
            ingredientes.setText(receta.getIngredientes());
            descripcion.setText(receta.getDescripcion());
            tiempo.setText(receta.getTiempo());
            if (receta.getImagen() != null) {
                if (receta.getImagen() != "") {
                    try {
                        Log.e("LOGG IMG", "imagen: " + receta.getImagen());
                        InputStream stream = getAssets().open(receta.getImagen());
                        Drawable d = Drawable.createFromStream(stream, null);
                        if (d !=null) {
                            collapsingToolbarLayout.setBackground(d);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
