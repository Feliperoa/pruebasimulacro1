package navdrawer.test.com.navigationdrawertest.model;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by Felipe on 13/07/2017.
 */

public class Receta extends SugarRecord {
    private String name;
    private String imagen;
    private String origen;
    private String descripcion;
    private String tiempo;
    private String categoria;
    private String file;
    private String ingredientes;

    public Receta() {
    }

    public Receta(String name, String imagen, String origen, String descripcion, String tiempo, String categoria, String file, String ingredientes) {
        this.name = name;
        this.imagen = imagen;
        this.origen = origen;
        this.descripcion = descripcion;
        this.tiempo = tiempo;
        this.categoria = categoria;
        this.file = file;
        this.ingredientes = ingredientes;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        
        this.ingredientes = ingredientes;
    }

    @Override
    public String toString() {
        return "Receta{" +
                "name='" + name + '\'' +
                ", imagen='" + imagen + '\'' +
                ", origen='" + origen + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", tiempo='" + tiempo + '\'' +
                ", ingredientes=" + ingredientes +
                '}';
    }
}
