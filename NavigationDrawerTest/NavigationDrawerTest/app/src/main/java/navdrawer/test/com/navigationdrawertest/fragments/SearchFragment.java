package navdrawer.test.com.navigationdrawertest.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;
import java.util.List;

import navdrawer.test.com.navigationdrawertest.R;
import navdrawer.test.com.navigationdrawertest.controller.DeeeeActivity;
import navdrawer.test.com.navigationdrawertest.model.Receta;
import navdrawer.test.com.navigationdrawertest.utils.adapter.RecetaAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {
    List<Receta> list;
    SearchView searchView;
    RecyclerView recyclerView;
    RecetaAdapter recetaAdapter;

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_search, container, false);
        searchView = (SearchView) v.findViewById(R.id.searchView);
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview);
        searchView.setIconified(false);
        list = new ArrayList<>();
        search();
        return v;
    }

    public void search() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                list.clear();
                List<Receta> all = Receta.listAll(Receta.class);
                if (all.size() > 0) {
                    for (int i=0; i<all.size();i++) {
                        Receta receta = all.get(i);
                        Log.e("LOGG", "receta: " + receta.toString());
                        String ingredientes = receta.getIngredientes();
                        if (ingredientes.toLowerCase().contains(query)) {
                           list.add(receta);
                        }
                    }
                    if (list.size() > 0) {
                        listBusqueda();
                    }
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }

    public void listBusqueda() {

        if (list.size() > 0) {
            recetaAdapter = new RecetaAdapter(list, getActivity(), new RecetaAdapter.OnItemClick() {
                @Override
                public void OnClick(Receta receta, int position) {
                    Log.e("LOGG", "esto cliqueo: " + receta.toString());
                    Intent intent = new Intent(getActivity(), DeeeeActivity.class);
                    intent.putExtra("id", String.valueOf(receta.getId()));
                    startActivity(intent);
                }
            });
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(recetaAdapter);
        } else {
        }
    }

}
