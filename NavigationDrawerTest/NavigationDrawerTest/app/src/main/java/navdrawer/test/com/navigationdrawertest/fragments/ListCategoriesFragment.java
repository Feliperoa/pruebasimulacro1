package navdrawer.test.com.navigationdrawertest.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import navdrawer.test.com.navigationdrawertest.controller.DeeeeActivity;
import navdrawer.test.com.navigationdrawertest.R;
import navdrawer.test.com.navigationdrawertest.model.Receta;
import navdrawer.test.com.navigationdrawertest.utils.adapter.RecetaAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListCategoriesFragment extends Fragment {
RecyclerView recyclerView;
RecetaAdapter recetaAdapter;
    List<Receta> lista;
    public ListCategoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_list_categories, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview);
        lista = new ArrayList<>();

        if (!NavigationDrawerFragment.category.equals("")) {
            listRcets(NavigationDrawerFragment.subcategory);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(NavigationDrawerFragment.category);
           /* switch (NavigationDrawerFragment.category) {
                case "comidasdiarias":
                    switch (NavigationDrawerFragment.subcategory) {
                        case "almuerzo":
                            listRcets("almuerzo");
                            break;
                        case "desayuno":
                            listRcets("desayuno");
                            break;
                        case "cena":
                            listRcets("cena");
                            break;

                    }
            }*/
        }

        return v;
    }

    public void listRcets(String categoty) {
        Log.e("LOFF", "categoria: " + categoty);
        lista = Receta.find(Receta.class, "categoria =?", new String[]{categoty});
       // lista = Receta.listAll(Receta.class);
        if (lista.size() > 0) {
            Log.e("LOGG", "la categoria sitiene  tiene " + lista.toString());
            recetaAdapter = new RecetaAdapter(lista, getContext(), new RecetaAdapter.OnItemClick() {
                @Override
                public void OnClick(Receta receta, int position) {
                    Log.e("LOGG", "esto cliqueo: " + receta.toString());
                    Intent intent = new Intent(getActivity(), DeeeeActivity.class);
                    intent.putExtra("id", String.valueOf(receta.getId()));
                    startActivity(intent);
                }
            });
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(recetaAdapter);

        } else {
            Log.e("LOGG", "la categoria no tiene ");
        }

    }

}
