Nombre de la receta:
TUILLES CON YOGURT

Lista de Ingredientes:
- 300g de arroz de grano largo (basmati ó jazmín...)
- 300g de pechuga de pollo
- 4 cucharadas de pasta de curry rojo tailandés
- 700ml de leche de coco
- 4-5 cucharadas de aceite de girasol
- sal

PREPARACIÓN:

Empezamos poniendo el aceite (cucharadas generosas) a calentar en una sartén amplia. Cuando esté caliente, echamos la pasta de curry y removemos bien
Añadimos la leche de coco y removemos para que se integre. Rectificamos de sal.
Con unas tijeras cortamos las pechugas en trozos no muy grandes, intentando no hacer unos mucho más grandes que otros.
Añadimos la pechuga a la salsa para que cueza ya con el sabor del curry thai.
Mientras ponemos a cocer el arroz en agua salada. Lo dejamos los 15-20 minutos y comprobamos la cocción.
Una vez cocido, colamos el arroz, para retirar todo el exceso de agua y lo reservamos.
Comprobamos la cocción del pollo (no conviene que quede crudo) y echamos el arroz que ya teníamos cocido.


TIEMPO DE PREPARACIÓN:
40 MINUTOS
ORIGEN:
Caldas Antioquia
IMAGEN:
t50.png
CATEGORÍA:
postres