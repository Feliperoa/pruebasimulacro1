Nombre de la receta:
SMOOTHIE DE FRUTOS ROJOS

Lista de Ingredientes:
- 400 g arroz basmati
- 3 huevos
- 1 zanahoria
- 250 g de jamón dulce en lonchas gruesas
- 150 g de gambas peladas
- 180 g de guisantes (1 lata)
- aceite de oliva
- pimienta negra molida
- sal

PREPARACIÓN:

Primero procedemos a hervir el arroz: Dos medidas de agua por cada medida de arroz, junto con un poco de sal, pimienta y un poco de aceite y lo dejamos a fuego lento. Una vez hecho lo escurrimos en agua frí­a y lo reservamos en un bol.

Con los huevos hacemos en otra sartén una tortilla francesa y la dejamos enfriar un poco. Una vez templada la cortamos en cuadraditos.

Cortamos la zanahoria y el jamón dulce en trocitos pequeños.

En una sartén grande con un poco de aceite salteamos las gambas hasta que se doren un poco.

Agregamos los guisantes, la zanahora y el jamón dulce a la sartén.

Cocinamos por 2 minutos dando vueltas, hasta que estén dorados

Agregamos el arroz y lo salteamos unos minutos dando vueltas.

Opcionalmente puedes agregar una cucharada de salsa de soja.


TIEMPO DE PREPARACIÓN:
45 MINUTOS
ORIGEN:
Caldas Antioquia
IMAGEN:
t16.png
CATEGORÍA:
bebidas
